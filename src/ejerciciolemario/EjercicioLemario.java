/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciolemario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xp
 */
public class EjercicioLemario 
{
    HashMap <String,Boolean> listaLemario = new HashMap();                      //Mi HashMap
    
    public static String normalizeString(String cadena){
	String limpiar = null;
	if (cadena !=null) {
        String valor = cadena;
        valor = valor.toLowerCase();
                                                                                //NormalizeString sirve para: Devolver una nueva cadena cuyo valor textual es igual que esta cadena, pero su representación binaria es de forma UNICODE especifica.
        limpiar = Normalizer.normalize(valor, Normalizer.Form.NFD);             // Normalizar texto para eliminar acentos, dieresis, cedillas y tildes
        
        limpiar = limpiar.replaceAll("[^\\p{ASCII}(N\u0303)(n\u0303)(\u00A1)"   // Quitar caracteres no ASCII excepto eñe, interrogacion que abre, exclamacion que abre, grados y dieresis.
                + "(\u00BF)(\u00B0)(U\u0308)(u\u0308)]", "");
        
        limpiar = Normalizer.normalize(limpiar, Normalizer.Form.NFC);           // Regresar a la forma compuesta, para poder comparar la enie con la tabla de valores
    }
    return limpiar;
    }    
      
    public void cargaFicheroLemario()
    {
        File fichero = new File("src/ejerciciolemario/lemario-20101017.txt");   //Pillar txt del Lemario
               
        try 
        {
            FileReader fr = new FileReader(fichero);                            //Lo carga en memoria    
            BufferedReader br = new BufferedReader(fr);                         //Lo de los USB
            String linea; 
            while((linea =  br.readLine()) != null)
            {
                listaLemario.put(normalizeString(linea.toLowerCase()), true);   //Para el HashMap
            }
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(EjercicioLemario.class.getName()).log(Level.SEVERE, 
                    null, ex);                                                  //Salida de erro en caso de qe no encuentre el fichero
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(EjercicioLemario.class.getName()).log(Level.SEVERE, 
                    null, ex);                                                  //No encuentre ningun metodo para hacer algo
        }      
    }
    
    public boolean existe(String palabra){
	try 
        {
	    if(listaLemario.get(normalizeString(palabra.toLowerCase())))
            {
                return true;                                                    //comprobacion de si esta la palabra en el HashMap
            }
            else
            {
                return false;
            }
	} 
        catch (Exception e) 
        {
	    return false;
	}
    }
    
    public boolean escalera(String[] escalera)                                  //Adaptación del código del ejercico de escaleras de palabras
    {
	boolean esEscalera = true;
	int contador;
	for(int i = 0; i < escalera.length - 1 && esEscalera; i++)
        {
	    contador = 0;                                                       //lo iniciamos en 0 y puesto aqui para que cada vuelta se reinicie
	    if(escalera[i].length() == escalera[i + 1].length())
            {
		if(existe(normalizeString(escalera[i].toLowerCase()))
                        && existe(normalizeString(escalera[i+1].toLowerCase())))//comprobación en el caso de que este la palabra dentro del HashMap
                {
		    for(int j = 0; j < escalera[i].length(); j++)               //Proceso de la escalera
                    {
			if(normalizeString(escalera[i].toLowerCase()).
                                charAt(j)!= normalizeString(escalera[i+1].
                                        toLowerCase()).charAt(j))
                        {
                            contador++;
			}
		    }   
		}
		else
                {
		    return false;
		}
		if(contador!=1)
                {
		   esEscalera = false;                                          //si es diferente de uno no es escalera
		}
	    }
	    else
            {
		esEscalera = false;
		return esEscalera;
	    }
	}
	return esEscalera;
    }
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EjercicioLemario comprueba = new EjercicioLemario();
        comprueba.cargaFicheroLemario();
        
        //------------------Ejercicio de búsqueda-------------------------------
        System.out.println("=================Lemario búsqueda=================");
        System.out.println("Busqueda palabras");
	System.out.println(comprueba.existe("HOLA"));
        System.out.println(comprueba.existe("HOLAAA"));
        System.out.println(comprueba.existe("hOlA"));
	System.out.println("=================================================");
        
        //------------------Ejercicio escalera----------------------------------
        System.out.println("=================Lemario escalera================");
	String[] prueba1 = {"dedo", "dedo", "dado", "daño", "maño"};
        String[] prueba2 = {"daño", "dado", "dadá", "dama", "data"};
	System.out.println(comprueba.escalera(prueba1));
        System.out.println(comprueba.escalera(prueba2));
	System.out.println("=================================================");
    }
    
}
